import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  @Input() disticts: Array<any>;
  @Output() selecteddistict = new EventEmitter<any>();
  selected: string;

  constructor() { }

  ngOnInit(): void {
  }

  /**
   * Emmiting selected location details to dashboard
   * @param location selected location
   */
  getGeoLocation(location) {
    this.selected = location;
    this.selecteddistict.emit(location);
  }
}
