import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LocationService  {

  constructor(private http: HttpClient) {  }

  // retrive all states
  public getAllStates(): Observable<any> {
    return this.http.get<any>('../../../../assets/states-and-districts.json')
    .pipe(map((res: any) => res.states));
  }
}
