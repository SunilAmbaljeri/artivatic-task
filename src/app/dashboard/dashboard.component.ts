import { Component, OnInit } from '@angular/core';
import { LocationService } from '../dashboard/common/helpers/location.service';
import * as _ from 'underscore';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  states: Array<any> = [];
  allStates: Array<any> = [];
  distictsOfStates: Array<any> = [];
  geoLocations: Array<any> = [];
  selectedDistict: any;
  selectState: string;

  constructor(private locationService: LocationService) { }

  ngOnInit(): void {
    this.initialize();
  }

  /**
   * Get all States on initial run
   */
  public initialize(): void {
    this.locationService.getAllStates().subscribe((response: any) => {
      this.allStates = response;
      this.states =  _.sortBy(response, (S) => {
        return S.state;
      });
    });
  }

  /**
   * Retreiving all the cities based on seleected State
   * @param selectedState selected dropdown output
   */
  public selectedState(selectedState): void {
    this.selectState = selectedState.state;
    this.distictsOfStates = _.sortBy(selectedState.districts, "district")
  }

  /**
   * Emmitted output of list component
   * @param distict selected distict
   */
  public onselectedDistict(distict): void {
    this.geoLocations = [];
    this.selectedDistict = distict;
    this.getCoordinator(distict);
  }

  /**
   * Get Geo Location by address
   * @param distict selected distict
   */
  public getCoordinator(distict): void {
    new google.maps.Geocoder().geocode({ address: distict }, (result, response) => {
      if (response === google.maps.GeocoderStatus.OK) {
        const lat = result[0].geometry.location.lat();
        const lng = result[0].geometry.location.lng();
        this.geoLocations = [...this.geoLocations, { lat, lng, distict }];
      }
    });
  }
}
